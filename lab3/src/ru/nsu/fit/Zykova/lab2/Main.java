package ru.nsu.fit.Zykova.lab2;

/* Laboratory work 3 for NSU
 * by Zykova Anastasya 16210
 */

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        MyThreadPool pool = new MyThreadPool(4);
        AtomicInteger a = new AtomicInteger(0);
        Runnable runnable = new Test(a);

        for (int i = 0; i < 1000; ++i) {
            pool.execute(runnable);
        }
        pool.join();

        System.out.println("A:" + a.get());
    }
}
