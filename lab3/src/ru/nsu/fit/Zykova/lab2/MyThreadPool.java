package ru.nsu.fit.Zykova.lab2;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class MyThreadPool implements Executor {

    private final ArrayList<Runnable> queue;
    private ArrayList<MyThread> threadList;

    MyThreadPool(int threadCount) {
        queue = new ArrayList<>();
        threadList = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            MyThread thread = new MyThread("T" + i, queue);
            thread.start();
            threadList.add(thread);
        }
    }

    @Override
    public void execute(Runnable command) {
        synchronized (queue) {
            queue.add(command);
            queue.notify();
        }
    }

    public void join() {
        while (true) {
            synchronized (queue) {
                if (queue.isEmpty()) {
                    break;
                }
            }
        }

        for (MyThread thread : threadList) {
            thread.cancel();
        }

        synchronized (queue) {
            queue.notifyAll();
        }

        for (MyThread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
