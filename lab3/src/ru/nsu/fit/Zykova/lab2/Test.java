package ru.nsu.fit.Zykova.lab2;

import java.util.concurrent.atomic.AtomicInteger;

public class Test implements Runnable {
    private AtomicInteger a;

    Test(AtomicInteger a) {
        this.a = a;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            a.incrementAndGet();
        }
    }
}
