package ru.nsu.fit.Zykova.lab2;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Response {
    private int status = 200;
    private String message = "OK";
    private Map<String, String> headers;
    private byte[] body;

    Response(byte[] body) {
        init(body);
    }

    private void init(byte[] body) {
        headers = new HashMap<>();
        headers.put("Content-Type", "text/html");
        this.body = body;
    }

    Response(ErrorCode errorCode) {
        init((String.valueOf(errorCode.code) +
                " " + errorCode.message).getBytes());
        setStatus(errorCode.code, errorCode.message);
    }

    private void setStatus(int statusCode, String statusMessage) {
        status = statusCode;
        message = statusMessage;
    }

    private String doString() throws MyException {
        StringBuilder result = new StringBuilder();
        String version = "HTTP/1.1";
        result.append(version).append(" ");
        result.append(String.valueOf(status))
                .append(" ")
                .append(message)
                .append("\n");
        try {
            for (Map.Entry<String, String> item : headers.entrySet()) {
                result.append(item.getKey())
                        .append(": ")
                        .append(item.getValue())
                        .append("\n");
            }
        } catch (NullPointerException e) {
            throw new MyException("Headers is absent");
        }
        try {
            result.append("\n").append(new StringBuilder(new String(body, "UTF-8")));
        } catch (UnsupportedEncodingException e ) {
            throw new MyException("fuck?");
        }
        return result.toString();
    }

    public byte[] getBytes() throws MyException {
        return doString().getBytes();
    }
}
