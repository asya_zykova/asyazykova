package ru.nsu.fit.Zykova.lab2;

/* Laboratory work 2 for NSU
 * by Zykova Anastasya 16210
 */

public class Main {
    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.start();
        } catch (MyException e) {
            System.out.println(e.giveDescription());
        }
    }
}
