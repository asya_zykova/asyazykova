package ru.nsu.fit.Zykova.lab2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

// Listens on port 8080 and displays
// all messages coming to it on the console

class Server {
    private static final int port = 8080;

    private Response answer(Request request) throws MyException, ErrorCode {
        FileProcessor fileProcessor = new FileProcessor(request.path);
        byte[] body = fileProcessor.searchBody();
        return new Response(body);
    }

    public void start() throws MyException {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new MyException("Couldn't connect to port");
        }

        // If the port was free and the socket was successfully created,
        // you can proceed to the expectation of the clone

        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                continue;
            }

            InputStream input = null;
            try {
                input = clientSocket.getInputStream();
            } catch (IOException e) {
                throw new MyException("Couldn't read from socket");
            }

            OutputStream output = null;
            try {
                output = clientSocket.getOutputStream();
            } catch (IOException e) {
                throw new MyException("Couldn't read from socket");
            }

            Response response = null;

            try {
                Request request = new Request(input);
                // Парсер пути
                // Обработчик + данные для обработчик
                // Обработчик(данные)
                // Ответ
                response = answer(request);
            } catch (ErrorCode errorCode) {
                response = new Response(errorCode);
            }

            try {
                output.write(response.getBytes());
            } catch (IOException e) {
                throw new MyException("Couldn't write to socket");
            }

            try {
                clientSocket.close();
            } catch (IOException e) {
                throw new MyException("Couldn't close socket");
            }
        }
    }
}
