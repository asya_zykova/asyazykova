package ru.nsu.fit.Zykova.lab2;

public class MyException extends Exception {
    private String description;

    MyException(String description) {
        this.description = description;
    }

    String giveDescription() {
        return this.description;
    }
}