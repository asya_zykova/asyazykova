package ru.nsu.fit.Zykova.lab2;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Request {
    public String path;
    private String method;
    private String version;

    private Map<String, String> headers;
    private BufferedInputStream bufferedInputStream;

    // Read the buffered string and turn it into a normal String

    private String readBufferedLine() throws IOException, ErrorCode {
        List<Byte> list = new ArrayList<>();
        int c;
        while (true) {
            c = bufferedInputStream.read();
            if ((c > 255) || (c < 0)) {
                throw new ErrorCode(500);
            }
            byte b = (byte) c;
            list.add(b);
            if (b == '\n') {
                StringBuilder res = new StringBuilder();
                list.remove(list.size() - 1);
                if (list.get(list.size() - 1) == '\r') {
                    list.remove(list.size() - 1);
                }
                for (Byte j : list) {
                    res.append((char) (byte) j);
                }
                return res.toString();
            }

        }
    }

    private void parseData() throws ErrorCode {
        String str;
        try {
            str = readBufferedLine();
        } catch (IOException e) {
            throw new ErrorCode(500);
        }

        String[] list = str.split(" ");
        if (list.length != 3) {
            throw new ErrorCode(500);
        }

        method = list[0];
        path = list[1];
        version = list[2];
    }

    private void validate() throws ErrorCode {
        if (!version.equals("HTTP/1.1")) {
            throw new ErrorCode(500);
        }

        if (!method.equals("GET")) {
            throw new ErrorCode(500);
        }

        if (!path.equals("/") && path.charAt(path.length() - 1) == '/') {
            throw new ErrorCode(404);
        }

        path = "static/" + path.replaceFirst("/", "");

    }

    private void createHeaders() throws ErrorCode {
        String str;
        while (true) {
            try {
                str = readBufferedLine();
                if (str.equals("")) {
                    break;
                }
            } catch (IOException e) {
                throw new ErrorCode(500);
            }
            String[] list = str.split(": ");
            headers.put(list[0], list[1]);
        }
    }

    Request(InputStream input) throws ErrorCode {
        bufferedInputStream = new BufferedInputStream(input);
        headers = new HashMap<>();
        parseData();
        validate();
        createHeaders();
    }
}
