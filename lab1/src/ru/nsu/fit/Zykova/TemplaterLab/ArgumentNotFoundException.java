package ru.nsu.fit.Zykova.TemplaterLab;

public class ArgumentNotFoundException extends Exception {
    private String description;

    public ArgumentNotFoundException(String description) {
        this.description = description;
    }

    String giveDescription() {
        return this.description;
    }
}
