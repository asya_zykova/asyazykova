package ru.nsu.fit.Zykova.TemplaterLab;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* Laboratory work NSU
 * by Zykova Anastasya 16210
 */

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "lab1/src/ru/nsu/fit/Zykova/TemplaterLab/template.txt")));
        String str;
        StringBuilder stringBuilder = new StringBuilder();
        str = reader.readLine();
        while (str!= null) {
            stringBuilder.append(str);
            str = reader.readLine();
        }

        Map<String, String> values = new HashMap<>();
        Map<String, Boolean> conditions = new HashMap<>();
        Map<String, Integer> iterations = new HashMap<>();

        values.put("name", "Roma");
        conditions.put("isUser", true);
        iterations.put("iteration", 4);

        Template template = new Template(stringBuilder.toString());
        str = template.fill(values, conditions, iterations);
        System.out.println(str);
    }
}
