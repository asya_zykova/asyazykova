package ru.nsu.fit.Zykova.TemplaterLab;

import ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.TemplateProcessor;
import java.util.ArrayList;
import java.util.Map;

class Template {
    private String string;

    Template(String string) {
        this.string = string;
    }

    String fill(Map<String, String> values,
                Map<String, Boolean> conditions,
                Map<String, Integer> iterations) {

        // Set the line parser
        Parser parser = new Parser(string);
        ArrayList<TemplateProcessor> listOfProcessors = new ArrayList<>();

        // And parse
        try {
            listOfProcessors = parser.parse(conditions);
        } catch (ArgumentNotFoundException ex) {
            System.err.println(ex.giveDescription());
        }

        // And fill the line
        StringBuilder builder = new StringBuilder();
        try {
            for (TemplateProcessor tp : listOfProcessors) {
                tp.fillTemplate(builder, values, conditions, iterations);
            }
        } catch (ArgumentNotFoundException ex) {
            builder.delete(0, builder.length());
            System.err.println(ex.giveDescription());
        }

        return builder.toString();
    }
}
