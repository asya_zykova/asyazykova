package ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor;

import ru.nsu.fit.Zykova.TemplaterLab.ArgumentNotFoundException;
import java.util.Map;

public class ValueProcessor implements
        ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.TemplateProcessor {
    private String string;

    public ValueProcessor(String string) {
        this.string = string;
    }

    @Override
    public void fillTemplate(StringBuilder sb,
                             Map<String, String> values,
                             Map<String, Boolean> conditions,
                             Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (values.isEmpty()) {
            throw new ArgumentNotFoundException("Values is absent");
        } else {
            sb.append(values.get(string));
        }
    }
}