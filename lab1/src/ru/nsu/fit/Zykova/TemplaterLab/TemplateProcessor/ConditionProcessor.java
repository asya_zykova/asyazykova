package ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor;

import java.util.Map;

public class ConditionProcessor implements
        ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.TemplateProcessor {
    private String string;
    private String condition;

    public ConditionProcessor(String string, String condition) {
        this.string = string;
        this.condition = condition;
    }

    @Override
    public void fillTemplate(StringBuilder sb,
                             Map<String, String> values,
                             Map<String, Boolean> conditions,
                             Map<String, Integer> iterations) {
        if (conditions.get(condition)) {
            sb.append(string);
        }
    }
}