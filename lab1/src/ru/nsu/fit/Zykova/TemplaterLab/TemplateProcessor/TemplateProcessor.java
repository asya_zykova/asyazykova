package ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor;

/* The Processor has a list of handlers:
 * ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.StringProcessor - just inserts a string
 * ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.ValueProcessor - inserts a value
 * ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.ConditionProcessor - inserts a string if the condition true
 * ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.RepeatProcessor - inserts a string n times
 * All handlers are inherited from the interface ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.TemplateProcessor
 */

import ru.nsu.fit.Zykova.TemplaterLab.ArgumentNotFoundException;
import java.util.Map;

public interface TemplateProcessor {

    void fillTemplate(StringBuilder sb,
                      Map<String, String> values,
                      Map<String, Boolean> conditions,
                      Map<String, Integer> iterations) throws ArgumentNotFoundException;
}