package ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor;

import ru.nsu.fit.Zykova.TemplaterLab.ArgumentNotFoundException;
import java.util.Map;

public class RepeatProcessor implements TemplateProcessor {
    private String string;
    private String iteration;

    public RepeatProcessor(String string, String iteration) {
        this.string = string;
        this.iteration = iteration;
    }

    @Override
    public void fillTemplate(StringBuilder sb,
                             Map<String, String> values,
                             Map<String, Boolean> conditions,
                             Map<String, Integer> iterations) throws ArgumentNotFoundException {
        if (iterations.isEmpty()) {
            throw new ArgumentNotFoundException("Iterations is absent");
        }
        for (int i = 0; i < iterations.get(iteration); i++) {
            sb.append(string);
        }
    }
}