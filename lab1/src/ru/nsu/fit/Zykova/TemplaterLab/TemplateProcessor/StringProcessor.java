package ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor;

import java.util.Map;

public class StringProcessor implements TemplateProcessor {
    private String string;

    public StringProcessor(String string){
        this.string = string;
    }

    @Override
    public void fillTemplate(StringBuilder sb,
                             Map<String, String> values,
                             Map<String, Boolean> conditions,
                             Map<String, Integer> iterations) {
        sb.append(string);
    }
}