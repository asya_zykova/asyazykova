package ru.nsu.fit.Zykova.TemplaterLab;

import ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.*;
import java.util.ArrayList;
import java.util.Map;

/* The parser parses the line in parts.
 * It has functions for recognizing a normal string, a value,
 * a condition, and iterations. Pass on the line is carried out once.
 */

public class Parser {
    private String string;

    Parser(String string) {
        this.string = string;
    }

    // Simple string parser, it is needed for parsing a simple string, as well as values

    private int parseString(int i, StringBuilder simpleString) {
        while (string.charAt(i) != '%') {
            simpleString.append(string.charAt(i));
            i++;
        }
        return i;
    }

    // Condition parser

    private int parseCondition(int i, Map<String, Boolean> conditions,
                               ArrayList<TemplateProcessor> listOfProcessors) throws ArgumentNotFoundException {
        i += 4;
        StringBuilder condition = new StringBuilder();
        while (string.charAt(i) != '!') {
            condition.append(string.charAt(i));
            i++;
        }
        i++;

        StringBuilder str = new StringBuilder();
        if ((!conditions.isEmpty()) && (conditions.containsKey(condition.toString())) && (string.charAt(i) == '%')) {
            i++;
            while (string.charAt(i) != '%') {
                str.append(string.charAt(i));
                i++;
            }
            i++;
        } else {
            throw new ArgumentNotFoundException("Invalid condition format");
        }

        String s = condition.toString();
        if (string.charAt(i) == '!') {
            condition.delete(0, condition.length());
            while (string.charAt(i) != '%') {
                condition.append(string.charAt(i));
                i++;
            }
            if (!condition.toString().equals("!endif!")) {
                throw new ArgumentNotFoundException("Invalid condition format, << !endif! is absent >>");
            }
        } else {
            throw new ArgumentNotFoundException("Invalid condition format");
        }

        listOfProcessors.add(new ConditionProcessor(str.toString(), s));
        i++;
        return i;
    }

    // Parse for iterations

    private int parseRepeat(int i, ArrayList<TemplateProcessor> listOfProcessors) throws ArgumentNotFoundException {
        i += 8;
        StringBuilder builder = new StringBuilder();
        while (string.charAt(i) != '!') {
            builder.append(string.charAt(i));
            i++;
        }
        i++;

        String iter = builder.toString();
        builder.delete(0, builder.length());
        String str;
        if (string.charAt(i) == '%') {
            i++;
            while (string.charAt(i) != '%') {
                builder.append(string.charAt(i));
                i++;
            }
            i++;

            str = builder.toString();
            builder.delete(0, builder.length());
            while (string.charAt(i) != '%') {
                builder.append(string.charAt(i));
                i++;
            }
            if (!builder.toString().equals("!endrepeat!")) {
                throw new ArgumentNotFoundException("Invalid iteration format");
            }
        } else {
            throw new ArgumentNotFoundException("Invalid iteration format");
        }

        listOfProcessors.add(new RepeatProcessor(str, iter));
        i++;
        return i;
    }

    // Parsing, return ArrayList of Processors how ru.nsu.fit.Zykova.TemplaterLab.TemplateProcessor.StringProcessor, Value, Condition or Repeat

    ArrayList<TemplateProcessor> parse(Map<String, Boolean> conditions) throws ArgumentNotFoundException {

        while (string.contains("\\%")) {
            this.string = string.replace("\\%", "%");
        }

        ArrayList<TemplateProcessor> listOfProcessors = new ArrayList<>();
        int i = 0;

        while (i < string.length()) {
            if (string.charAt(i) != '%') {
                StringBuilder simpleString = new StringBuilder();
                i = parseString(i, simpleString);
                listOfProcessors.add(new StringProcessor(simpleString.toString()));
                continue;
            } else {
                i++;
            }

            if (string.charAt(i) != '!') {
                StringBuilder simpleString = new StringBuilder();
                i = parseString(i, simpleString);
                listOfProcessors.add(new ValueProcessor(simpleString.toString()));
                i++;
                continue;
            }

            if ((string.charAt(i) == '!') && ((string.substring(i + 1, i + 3)).equals("if"))) {
                i = parseCondition(i, conditions, listOfProcessors);
                continue;
            }

            if ((string.charAt(i) == '!') && ((string.substring(i + 1, i + 7)).equals("repeat"))) {
                i = parseRepeat(i, listOfProcessors);
            } else {
                i++;
            }
        }
        return listOfProcessors;
    }
}