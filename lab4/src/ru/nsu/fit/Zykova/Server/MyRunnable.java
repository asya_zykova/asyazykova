package ru.nsu.fit.Zykova.Server;

import ru.nsu.fit.Zykova.Server.Handler.HTTPHandler;
import ru.nsu.fit.Zykova.Server.Handler.HTTPHandlerError;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class MyRunnable implements Runnable {
    Response response;
    Socket clientSocket;
    Server server;
    ServerContext context;

    public MyRunnable(Server server, Socket clientSocket) {
        this.server = server;
        this.context = server.context;
        this.clientSocket = clientSocket;
    }
    @Override
    public void run() {
        try {
            _run();
        } catch (MyException e) {
            System.out.println(e.giveDescription());
        }
    }

    public void _run() throws MyException {
        InputStream input = null;
        try {
            input = clientSocket.getInputStream();
        } catch (IOException e) {
            throw new MyException("Couldn't read from socket");
        }

        HTTPHandler handler = null;
        Request request = null;
        try {
            request = new Request(input);
            request = context.pipelineIn.interceptor(request);
            handler = context.getHandler(request);

        } catch (ErrorCode errorCode) {
            handler = new HTTPHandlerError(errorCode);
        }

        response = handler.handle(request);
        response = context.pipelineOut.interceptor(response);


        OutputStream output = null;
        try {
            output = clientSocket.getOutputStream();
        } catch (IOException e) {
            throw new MyException("Couldn't read from socket");
        }

        try {
            output.write(response.getBytes());
        } catch (IOException e) {
            throw new MyException("Couldn't write to socket");
        }

        try {
            clientSocket.close();
        } catch (IOException e) {
            throw new MyException("Couldn't close socket");
        }
    }
}
