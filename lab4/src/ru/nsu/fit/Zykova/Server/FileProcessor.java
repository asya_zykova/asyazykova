package ru.nsu.fit.Zykova.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

class FileProcessor {
    private String path;

    FileProcessor(String path) {
        this.path = path;
    }

    public byte[] searchBody() throws ErrorCode, MyException {
        File file = new File(path);
        if (!file.exists() || file.isDirectory()) {
            file = new File(path + (path.endsWith("/") ? "" : "/") + "index.html");
        }
        if(!file.exists() || file.isDirectory()) {
            throw new ErrorCode(404);
        }
        try {
            return new FileInputStream(file).readAllBytes();
        } catch (IOException e) {
            throw new MyException("Can't read file");
        }
    }
}
