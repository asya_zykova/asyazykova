package ru.nsu.fit.Zykova.Server.Matchers;

import java.util.Map;

public class StringMatcher implements SegmentMatcher {
    private String segment;
    private String name;

    public StringMatcher(String segment, String name) {
        this.segment = segment;
        this.name = name;
    }

    @Override
    public boolean selfEquals(SegmentMatcher other) {
        return (((StringMatcher)other).segment.equals(this.segment) &&
                ((StringMatcher)other).name.equals(this.name));
    }

    @Override
    public int priority() {
        return 20;
    }

    @Override
    public boolean match(String value, Map<String, String> pathInfo) {
        if (!value.equals(segment)) {
            return false;
        }
        if (name != null) {
            pathInfo.put(name, value);
        }
        return true;
    }
}
