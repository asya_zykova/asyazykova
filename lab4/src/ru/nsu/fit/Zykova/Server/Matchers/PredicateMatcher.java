package ru.nsu.fit.Zykova.Server.Matchers;

import java.util.Map;
import java.util.function.Predicate;

public class PredicateMatcher implements SegmentMatcher {
    private Predicate<String> predicate;
    private String name;

    public PredicateMatcher(Predicate<String> predicate, String name) {
        this.predicate = predicate;
        this.name = name;
    }

    @Override
    public boolean selfEquals(SegmentMatcher other) {
        return ((PredicateMatcher) other).predicate.equals(this.predicate);
    }

    @Override
    public int priority() {
        return 10;
    }

    @Override
    public boolean match(String value, Map<String, String> pathInfo) {
        if (!predicate.test(value)) {
            return false;
        }
        if (name != null) {
            pathInfo.put(name, value);
        }
        return true;
    }
}
