package ru.nsu.fit.Zykova.Server.Matchers;

import java.util.Map;

public class IntMatcher implements SegmentMatcher {
    private final String name;

    public IntMatcher(String name) {
        this.name = name;
    }

    @Override
    public boolean selfEquals(SegmentMatcher other) {
        return ((IntMatcher)other).name.equals(this.name);
    }

    @Override
    public int priority() {
        return 30;
    }

    @Override
    public boolean match(String value, Map<String, String> pathInfo) {
        try {
            int i = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return false;
        }

        if (name != null) {
            pathInfo.put(name, value);
        }
        return true;
    }
}
