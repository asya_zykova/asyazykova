package ru.nsu.fit.Zykova.Server.Matchers;

import java.util.Map;

public interface SegmentMatcher {
    default boolean equals(SegmentMatcher other) {
        if (other.getClass() == this.getClass()) {
            return selfEquals(other);
        }
        return false;
    }

    boolean selfEquals(SegmentMatcher other);

    int priority();

    boolean match(String value, Map<String, String> pathInfo);
}
