package ru.nsu.fit.Zykova.Server.Matchers;

import java.util.Map;

public class AnyMatcher implements SegmentMatcher {
    private String name;

    public AnyMatcher(String name) {
        this.name = name;
    }

    @Override
    public boolean selfEquals(SegmentMatcher other) {
        return ((AnyMatcher)other).name.equals(this.name);
    }

    @Override
    public int priority() {
        return 0;
    }

    @Override
    public boolean match(String value, Map<String, String> pathInfo) {
        if (name != null) {
            pathInfo.put(name, value);
        }
        return true;
    }
}
