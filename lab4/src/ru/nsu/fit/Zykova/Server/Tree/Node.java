package ru.nsu.fit.Zykova.Server.Tree;


import ru.nsu.fit.Zykova.Server.Handler.HTTPHandler;
import ru.nsu.fit.Zykova.Server.Matchers.SegmentMatcher;
import java.util.ArrayList;
import java.util.List;

public class Node {
    SegmentMatcher segmentMatcher;
    List<Node> childrens;
    HTTPHandler handler;

    Node(SegmentMatcher segmentMatcher) {
        this.segmentMatcher = segmentMatcher;
        childrens = new ArrayList<>();
    }

    private Node find(SegmentMatcher segmentMatcher) {
        for (Node node : childrens) {
            if (node.segmentMatcher.equals(segmentMatcher)) {
                return node;
            }
        }
        return null;
    }

    Node add(SegmentMatcher segmentMatcher) {
        Node node = find(segmentMatcher);

        if (node == null) {
            node = new Node(segmentMatcher);
            childrens.add(node);
            childrens.sort(new NodeComparator().reversed());
        }
        return node;
    }

    int priority() {
        return segmentMatcher.priority();
    }

    void attachHandler(HTTPHandler handler) {
        this.handler = handler;
    }
}
