package ru.nsu.fit.Zykova.Server.Tree;

import ru.nsu.fit.Zykova.Server.Handler.HTTPHandler;
import ru.nsu.fit.Zykova.Server.Handler.HTTPHandlerError;
import ru.nsu.fit.Zykova.Server.Matchers.SegmentMatcher;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Tree {
    private Root root;

    public Tree() {
        root = new Root();
    }

    public void addMatcherList(List<SegmentMatcher> matchers, HTTPHandler handler) {
        Node node = root;
        for (SegmentMatcher matcher : matchers) {
            node = node.add(matcher);
        }
        node.attachHandler(handler);
    }

    private HTTPHandler getHandler(List<String> pathSegments, Map<String, String> pathInfo) {
        HTTPHandler handler = deepSearch(root, pathSegments, 0, pathInfo);
        if (handler == null) {
            return new HTTPHandlerError(404);
        }
        return handler;
    }

    private HTTPHandler deepSearch(Node node, List<String> pathSegments,
                                   int index, Map<String, String> pathInfo) {
        if ((pathSegments.size() == index)) {
            if (node.handler != null) {
                return node.handler;
            } else {
                return null;
            }
        }

        if (!node.childrens.isEmpty()) {
            for (Node children : node.childrens) {
                if (children.segmentMatcher.match(pathSegments.get(index), pathInfo)) {
                    HTTPHandler handler = deepSearch(children, pathSegments, index + 1, pathInfo);
                    if (handler != null) {
                        return handler;
                    }
                }
            }
        }

        return null;
    }

    public HTTPHandler getHandler(String[] strings, Map<String, String> pathInfo) {
        return getHandler(Arrays.asList(strings), pathInfo);
    }

    public void clear() {
        root = new Root();
    }
}
