package ru.nsu.fit.Zykova.Server.Pipeline;
import ru.nsu.fit.Zykova.Server.Response;

import java.util.ArrayList;
import java.util.List;

public class PipelineOutcoming {
    List<PipelineStageOut> stages = new ArrayList<>();

    public void addStageEnd(PipelineStageOut stage) {
        stages.add(stage);
    }

    public void addStageStart(PipelineStageOut stage) {
        stages.add(0, stage);
    }

    public void addStageAfter(String name, PipelineStageOut stage) {
        for (int i = 0; i < stages.size(); i++) {
            if (name.equals(stages.get(i).getName())) {
                stages.add(i + 1, stage);
            }
        }
    }

    public void addStageBefore(String name, PipelineStageOut stage) {
        for (int i = 0; i < stages.size(); i++) {
            if (name.equals(stages.get(i).getName())) {
                stages.add(i, stage);
            }
        }
    }

    public Response interceptor(Response response) {
        for (PipelineStageOut stage : stages) {
            response = stage.interceptor(response);
        }
        return response;
    }

    public void clear() {
        stages.clear();
    }
}
