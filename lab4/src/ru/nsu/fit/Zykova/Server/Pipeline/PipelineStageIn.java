package ru.nsu.fit.Zykova.Server.Pipeline;

import ru.nsu.fit.Zykova.Server.Request;

public interface PipelineStageIn {
    public String getName();
    public Request interceptor(Request request);
}
