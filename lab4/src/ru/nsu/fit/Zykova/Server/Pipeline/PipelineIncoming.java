package ru.nsu.fit.Zykova.Server.Pipeline;

import ru.nsu.fit.Zykova.Server.Request;

import java.util.ArrayList;
import java.util.List;

public class PipelineIncoming {
    List<PipelineStageIn> stages = new ArrayList<>();
    public void addStageEnd(PipelineStageIn stage) {
        stages.add(stage);
    }

    public void addStageStart(PipelineStageIn stage) {
        stages.add(0, stage);
    }

    public void addStageAfter(String name, PipelineStageIn stage) {
        for (int i = 0; i < stages.size(); i++) {
            if (name.equals(stages.get(i).getName())) {
                stages.add(i + 1, stage);
            }
        }
    }

    public void addStageBefore(String name, PipelineStageIn stage) {
        for (int i = 0; i < stages.size(); i++) {
            if (name.equals(stages.get(i).getName())) {
                stages.add(i, stage);
            }
        }
    }

    public Request interceptor(Request request) {
        for (PipelineStageIn stage : stages) {
            request = stage.interceptor(request);
        }
        return request;
    }

    public void clear() {
        stages.clear();
    }
}
