package ru.nsu.fit.Zykova.Server.Pipeline;

import ru.nsu.fit.Zykova.Server.Response;

public interface PipelineStageOut {
    public String getName();
    public Response interceptor(Response response);
}
