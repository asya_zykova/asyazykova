package ru.nsu.fit.Zykova.Server.Handler;

import ru.nsu.fit.Zykova.Server.ErrorCode;
import ru.nsu.fit.Zykova.Server.Request;
import ru.nsu.fit.Zykova.Server.Response;

public class HTTPHandlerError extends HTTPHandler {

    ErrorCode errorCode;
    int code;
    String message;

    public HTTPHandlerError(int code) {
        this.code = code;
    }

    public HTTPHandlerError(ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.code = errorCode.code;
        this.message = errorCode.message;
    }

    @Override
    public Response handle(Request request) {
        StringBuilder builder = new StringBuilder();
        builder.append("<h1> ").append(code).append(" </h1> ").append(message);
        return new Response(builder.toString().getBytes(), request);

    }
}
