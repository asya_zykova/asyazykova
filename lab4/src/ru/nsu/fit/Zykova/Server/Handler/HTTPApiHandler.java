package ru.nsu.fit.Zykova.Server.Handler;

import ru.nsu.fit.Zykova.Server.Request;
import ru.nsu.fit.Zykova.Server.Response;

public class HTTPApiHandler extends HTTPHandler{
    @Override
    public Response handle(Request request) {
        StringBuilder s = new StringBuilder("path info:<br>");
        for (String key : request.pathInfo.keySet()) {
            s.append(key).append(":").append(request.pathInfo.get(key)).append("<br>");
        }
        return new Response(s.toString().getBytes(), request);
    }
}
