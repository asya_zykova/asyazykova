package ru.nsu.fit.Zykova.Server.Plugin;

import ru.nsu.fit.Zykova.Server.ServerContext;

public interface ServerPlugin {
    void initialize(ServerContext context);
    String getName();
}
