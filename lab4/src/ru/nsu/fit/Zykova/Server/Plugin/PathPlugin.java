package ru.nsu.fit.Zykova.Server.Plugin;

import ru.nsu.fit.Zykova.Server.Handler.HTTPApiHandler;
import ru.nsu.fit.Zykova.Server.Matchers.*;
import ru.nsu.fit.Zykova.Server.ServerContext;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PathPlugin implements ServerPlugin{

    @Override
    public void initialize(ServerContext context) {
        List<SegmentMatcher> sm = new ArrayList<>();
        sm.add(new StringMatcher("user", "api"));
        sm.add(new IntMatcher("id"));
        sm.add(new AnyMatcher("action"));
        context.addMatcherList(sm, new HTTPApiHandler());
//            /user/<id:int>/<action:any string>

        sm.clear();
        sm.add(new StringMatcher("user", "api"));
        sm.add(new IntMatcher("id"));
        Predicate<String> pred = s -> s.substring(0, 3).equals("del");
        sm.add(new PredicateMatcher(pred, "pred_action"));
        context.addMatcherList(sm, new HTTPApiHandler());
//            /user/<id:int>/del<any string>

        sm.clear();
        sm.add(new StringMatcher("user", "api"));
        sm.add(new StringMatcher("show", "action"));
        context.addMatcherList(sm, new HTTPApiHandler());
//            /user/show
    }

    @Override
    public String getName() {
        return "App";
    }
}
