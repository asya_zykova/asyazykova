package ru.nsu.fit.Zykova.Server.Plugin;

import ru.nsu.fit.Zykova.Server.Pipeline.PipelineStageIn;
import ru.nsu.fit.Zykova.Server.Pipeline.PipelineStageOut;
import ru.nsu.fit.Zykova.Server.Request;
import ru.nsu.fit.Zykova.Server.Response;
import ru.nsu.fit.Zykova.Server.ServerContext;

public class TimePlugin implements ServerPlugin {
    @Override
    public void initialize(ServerContext context) {
        PipelineStageIn msStart = new PipelineStageIn() {
            @Override
            public String getName() {
                return "msStart";
            }

            @Override
            public Request interceptor(Request request) {
                request.pathInfo.put("startMs", String.valueOf(System.currentTimeMillis()));
                return request;
            }
        };
        context.pipelineIn.addStageStart(msStart);

        PipelineStageOut msStop = new PipelineStageOut() {
            @Override
            public String getName() {
                return "msStop";
            }

            @Override
            public Response interceptor(Response response) {
                String str = response.request.pathInfo.get("startMs");
                Integer ms = (int) (System.currentTimeMillis() - Long.parseLong(str));
                String body = new String(response.body) + "<hr>ms: " + ms.toString();
                response.body = body.getBytes();
                return response;
            }
        };

        context.pipelineOut.addStageEnd(msStop);
    }

    @Override
    public String getName() {
        return "Time";
    }
}
