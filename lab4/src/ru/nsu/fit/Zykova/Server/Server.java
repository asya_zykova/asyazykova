package ru.nsu.fit.Zykova.Server;

import ru.nsu.fit.Zykova.Server.MyThreadPool.MyThreadPool;
import ru.nsu.fit.Zykova.Server.Plugin.PathPlugin;
import ru.nsu.fit.Zykova.Server.Plugin.ServerPlugin;
import ru.nsu.fit.Zykova.Server.Plugin.TimePlugin;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

// Listens on port 8080 and displays
// all messages coming to it on the console

public class Server implements ServerBootstrapper{
    private static final int port = 8080;
    MyThreadPool pool;
    final ServerContext context;
    List<ServerPlugin> plugins = new ArrayList<>();
    ServerSocket serverSocket;

    public Server(int threadCount) {
        pool = new MyThreadPool(threadCount);
        context = new ServerContext(this);
    }

    public ServerContext cntx() {
        return context;
    }

    public void start() throws MyException {
        for (ServerPlugin plugin : plugins) {
            plugin.initialize(context);
        }

        try {
            serverSocket = new ServerSocket(port);
            System.out.print("Connect to socket established, port:" + port);
        } catch (IOException e) {
            throw new MyException("Couldn't connect to port");
        }

        // If the port was free and the socket was successfully created,
        // you can proceed to the expectation of the clone

        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                continue;
            }

            MyRunnable runnable = new MyRunnable(this, clientSocket);
            pool.execute(runnable);
        }


    }

    public void restart() throws MyException, IOException {
        stop();
        start();
    }

    public void stop() throws IOException {
        pool.join();
        serverSocket.close();
        context.clear();
    }

    public void registerPlugin(ServerPlugin p) {
        plugins.add(p);
    }
}
