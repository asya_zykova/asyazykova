package ru.nsu.fit.Zykova.Server;

import ru.nsu.fit.Zykova.Server.Plugin.ServerPlugin;

import java.io.IOException;

interface ServerBootstrapper {
    ServerContext cntx();
    void start() throws MyException;
    void restart() throws MyException, IOException;
    void stop() throws IOException;
    void registerPlugin(ServerPlugin p);
}
