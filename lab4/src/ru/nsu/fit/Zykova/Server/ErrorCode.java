package ru.nsu.fit.Zykova.Server;

public class ErrorCode extends Exception {
    public int code;
    public String message;

    ErrorCode(int code) {
        this.code = code;
        switch (code) {
            case 404: {
                message = "Not Found";
                break;
            }
            case 500: {
                message = "InternalServerError";
                break;
            }
            case 405: {
                message = "Method Not Allowed";
                break;
            }
            default: {
                message = "Unknown Error";
                break;
            }
        }
    }
}
