package ru.nsu.fit.Zykova.Server;

public class MyException extends Exception {
    private String description;

    MyException(String description) {
        this.description = description;
    }

    public String giveDescription() {
            return this.description;
    }
}