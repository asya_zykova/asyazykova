package ru.nsu.fit.Zykova.Server.MyThreadPool;

import java.util.ArrayList;

public class MyThread extends Thread {
    private final ArrayList<Runnable> queue;
    private boolean cancelled = false;

    MyThread(String name, ArrayList<Runnable> queue) {
        this.setName(name);
        this.queue = queue;
    }

    @Override
    public void run() {
        while (!cancelled) {
            Runnable runnable;
            synchronized (queue) {
                if (queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
                runnable = queue.get(0);
                queue.remove(0);
            }
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public void cancel() {
        cancelled = true;
    }
}
