package ru.nsu.fit.Zykova.Server;

import ru.nsu.fit.Zykova.Server.Handler.HTTPHandler;
import ru.nsu.fit.Zykova.Server.Matchers.SegmentMatcher;
import ru.nsu.fit.Zykova.Server.Pipeline.PipelineIncoming;
import ru.nsu.fit.Zykova.Server.Pipeline.PipelineOutcoming;
import ru.nsu.fit.Zykova.Server.Tree.Tree;

import java.util.List;

public class ServerContext {
    private final Tree tree = new Tree();
    public PipelineIncoming pipelineIn = new PipelineIncoming();
    public PipelineOutcoming pipelineOut = new PipelineOutcoming();

    final ServerBootstrapper server;

    ServerContext(ServerBootstrapper server) {
        this.server = server;
    }

    public void addMatcherList(List<SegmentMatcher> matchers, HTTPHandler handler) {
        tree.addMatcherList(matchers, handler);
    }

    public HTTPHandler getHandler(Request request) {
        return tree.getHandler(request.pathSegments(), request.pathInfo);
    }

    void clear() {
        pipelineIn.clear();
        pipelineOut.clear();
        tree.clear();
    }
}
