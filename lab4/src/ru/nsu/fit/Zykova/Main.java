package ru.nsu.fit.Zykova;

/* Laboratory work 2 for NSU
 * by Zykova Anastasya 16210
 */

import ru.nsu.fit.Zykova.Server.MyException;
import ru.nsu.fit.Zykova.Server.Plugin.PathPlugin;
import ru.nsu.fit.Zykova.Server.Plugin.ServerPlugin;
import ru.nsu.fit.Zykova.Server.Plugin.TimePlugin;
import ru.nsu.fit.Zykova.Server.Server;

public class Main {
    public static void main(String[] args) {
        Server server = new Server(4);
        try {
            ServerPlugin pathPlugin = new PathPlugin();
            ServerPlugin timePlugin = new TimePlugin();

            server.registerPlugin(pathPlugin);
            server.registerPlugin(timePlugin);
            server.start();
        } catch (MyException e) {
            System.out.println(e.giveDescription());
        }
    }
}
